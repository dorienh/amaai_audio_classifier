import numpy as np
import pandas as pd

from sklearn.linear_model import LinearRegression
from preprocessing import getParticipantData, getParticipantDFColumns, getData, getDiscreteEmotions, modality_processing, getAV, timestamp_processing
from models import get_kfold_cross_validation, lm_scoring
from dataload import load_data_lm, scale_lm_features

"""
Generate three separate dataframes.
    1. participant_df: contains participant information
    2. media_df: cotains discrete emotion annotations
    3. av_df: cotains continuous arousal-valence annotations
"""
#define df colnames
emotions = pd.Series(['moved','nervous','filled_with_wonder','allured', 'fascinated','overwhelmed','feeling_of_transcendence',
                      'serene','calm','soothed','tender','affectionate','mellow',
                      'nostalgic','sentimental','dreamy','strong','energetic','triumphant',
                      'animated','bouncy','joyful',
                      'sad','tearful','blue','tense','agitated'])

mediaData_colnames = pd.concat([pd.Series(['participant_id','media_id','media_modality','seen_previously']), emotions])
accepted_modalities = pd.Series(['music_irrelevant', 'muted_video_true', 'video_true', 'video_false', 'muted_video_false'])

#generate dfs
filedir = 'data/participants'
participant_df, media_df, av_df, timestamps  = [], [], [], []

for f in os.listdir(filedir):
    filepath = os.path.join(filedir, f)
    participant_df.append(getParticipantData(filepath))

    data = getDiscreteEmotions(filepath)
    for row in data:
        media_df.append(row)

    data, timestamp = getAV(filepath)
    for row in data:
        av_df.append(row)

    for t in timestamp:
        timestamps.append(t)

#generates participant df
participant_df = pd.DataFrame(participant_df, columns=getParticipantDFColumns(filepath))

#generates media df, tidies modality column (music, video, muted_video)
media_df = pd.DataFrame(media_df, columns=mediaData_colnames)
media_df = modality_processing(media_df).replace({'true': 1, 'false': 0})

#generates arousal-valence df, tidies modality column
av_df = pd.DataFrame(av_df, columns = ['participant_id','media_id','media_modality','arousal','valence'])
av_df['timestamp'] = timestamps
av_df[['arousal', 'valence','timestamp']] = av_df[['arousal', 'valence','timestamp']].apply(pd.to_numeric)
av_df = modality_processing(av_df).drop_duplicates(['participant_id', 'media_id','timestamp'])
av_df = timestamp_processing(av_df)


"""
Example code for cross-validation of linear models for continuous arousal-valence annotations
"""
#read csv containing video_id
urls = pd.read_csv('data/video_urls.csv')
opensmileFeatureDir = 'data/emobase'

kf = get_kfold_cross_validation()
results = []
i=0

for train_index, val_index in kf.split(urls):
    X_train, y_arousal, y_valence = load_data_lm(av_df, urls.iloc[train_index], opensmileFeatureDir, 'music')
    X_val, y_val_arousal, y_val_valence = load_data_lm(av_df, urls.iloc[test_index], opensmileFeatureDir, 'music')

    X_train, X_val = scale_lm_features(X_train, X_val)

    #fit separate linear models for arousal and valence
    lm_arousal = LinearRegression().fit(X_train, y_arousal)
    lm_valence = LinearRegression().fit(X_train, y_valence)

    #obtain validation results
    r2_arousal, mse_arousal, r_arousal, ccc_a = lm_scoring(lm_arousal, X_val, y_val_arousal)
    r2_valence, mse_valence, r_valence, ccc_v = lm_scoring(lm_valence, X_val, y_val_valence)

    results.append([r2_arousal, mse_arousal, r_arousal, ccc_a, r2_valence, mse_valence, r_valence, ccc_v])

results = pd.DataFrame(results, columns=['r2_arousal', 'mse_arousal', 'r_arousal', 'ccc_a', 'r2_valence', 'mse_valence', 'r_valence',' ccc_v'])
