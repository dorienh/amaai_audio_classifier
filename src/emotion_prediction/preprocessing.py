import numpy as np
import pandas as pd
import os
from datetime import timedelta


def getParticipantDFColumns(file):
    """
    generate columns for participant DataFrame
    """
    f = open(file, "r")
    header = f.readline()
    f.close()

    header = header.split(',')
    participant_colnames = header[0].split(';')[0:9]
    participant_colnames.append('overlay_pref')

    return participant_colnames


def getData(file):
    """
    define generic file-opening code for data collected from scripts
    """
    f = open(file, 'r')
    f.readline()
    data = f.readline()
    f.close()

    data = data.split(',')
    participant_id = data[0].split(';')[0]

    return data, participant_id


def getParticipantData(file):
    """
    generate df with participant data
    """
    data, id = getData(file)

    participant_data = data[0].split(';')[0:9]
    overlay_pref = data[len(data)-1].split(';')[1]
    participant_data.append(overlay_pref)

    return participant_data


#generate media DataFrame
def getDiscreteEmotions(file): #can use pd.melt to obtain tidy DataFrame
    """
    generate df with participant's discrete emotion annotations for each song
    """
    data, participant_id = getData(file)

    mediaData=[]

    #get content ids for media watched by each participant
    idx_media_id = [data.index(s) for s in data if "nervous" in s]

    for j in range(1, len(idx_media_id)-1):
        media_data = data[idx_media_id[j]].split(';')[1]
        media_id = media_data.split('_', 1)[0]
        media_modality = media_data.split('_', 1)[1]
        media_list = [participant_id, media_id, media_modality]

        #get emotion ratings for each piece of media
        idx = [data.index(s) for s in data if "moved" in s]

        seen_previously = data[idx[j]].split(';')[2]
        moved = (data[idx[j]].split(';')[3]).split('_')[1]
        nervous = (data[idx[j]+26].split(';')[0]).split('_')[1]

        emotion_list = [seen_previously, moved, nervous]

        for l in range(1,26):
            emotion = data[idx[j]+l].split('_')[1]
            emotion_list.append(emotion)

        emotion_list = media_list + emotion_list
        mediaData.append(emotion_list)

    return mediaData


def modality_processing(df):
    """
    define accepted modalities for modality processing function to catch exceptions in raw data
    and remove overlay information trailing modality information
    """
    i = 5

    while i > 0:
        idx = df[-df['media_modality'].isin(accepted_modalities)].index

        if len(idx) == 0:
            break

        new_col = []

        for j in range(len(df)):
            if j in idx:
                new_modality = df['media_modality'][j].split('_',1)[1]
            else:
                new_modality = df['media_modality'][j]

            new_col.append(new_modality)

        df['media_modality'] = new_col
        i += -1

    #remove overlay information trailing modality information
    new_col = []

    for i in range(len(df)):
        new_modality = df.media_modality[i].split('_')
        del new_modality[-1]
        new_modality = '_'.join(new_modality)

        new_col.append(new_modality)

    df['media_modality'] = new_col
    return df


def getAV(file):
    """
    generate df with participant's continuous arousal-valence emotion annotations for each song
    """
    data, participant_id = getData(file)

    media_av, timestamps = [], []

    #get media ids
    idx_media_id = [data.index(s) for s in data if "nervous" in s]
    data = data[idx_media_id[1]:len(data)] #remove if screws up
    idx_media_id = [data.index(s) for s in data if "nervous" in s] #reset media_id indexes

    for i in range(len(idx_media_id)-1):
        first_index = idx_media_id[i]
        second_index = idx_media_id[i+1]

        dist = second_index - first_index
        chunk = data[(first_index):(second_index - 25)]

        media_data = data[first_index].split(';')[1]
        media_id = media_data.split('_', 1)[0]
        media_modality = media_data.split('_', 1)[1]

        #get timestamps
        timestamp_length = (dist-26)/2
        media_timestamp = chunk[0:int(timestamp_length)]
        media_timestamp[0] = media_timestamp[0].split(';')[2]
        timestamps.extend(media_timestamp)

        #parse first entry separately, formatting is different
        AV_raw = chunk[int(timestamp_length):len(chunk)]

        valence = AV_raw[0].split(';')[1]
        arousal = AV_raw[1].split('|')[0]
        media_av.append([participant_id, media_id, media_modality, arousal, valence])

        for j in range(1, len(AV_raw)-1):
            valence = AV_raw[j].split('|')[1]
            arousal = AV_raw[j+1].split('|')[0]
            media_av.append([participant_id, media_id, media_modality, arousal, valence])

    return media_av, timestamps


def timestamp_processing(av_df):
    """
    process timestamp stutters in raw data (caused by participant's internet connection)
    drop duplicate rows and resamples arousal-valence values to 0.5Hz
    """
    new_samples, seq = [], []

    participant_id_list = av_df['participant_id'].unique()
    media_id_list = av_df['media_id'].unique()

    #generate timedelta index
    av_df['timedelta'] = [timedelta(seconds=x) for x in av_df['timestamp']]
    df_ = av_df.set_index('timedelta')

    for i in participant_id_list:
        for j in media_id_list:
            group = df_[(df_.participant_id == i) & (df_.media_id == j)]

            if len(group) > 121:

                #merge and keep left join only
                df_ = df_.merge(group, how='left', indicator=True)
                df_ = df_[df_._merge == 'left_only']
                df_ = df_.drop(columns=['_merge'])

                #reset timedelta index
                df_['timedelta'] = [timedelta(seconds=x) for x in df_['timestamp']]
                df_ = df_.set_index('timedelta')

                #resample group
                group = group.resample('0.5S').first()
                new_samples.append(group)

    new_samples.append(df_)
    df_ = pd.concat(new_samples)
    df_ = df_.dropna(how='all')
    df_ = df_.reset_index(drop=True)

    #generate sequence index - necessary because timestamps aren't taken consistently at 0.5s intervals
    for i in participant_id_list:
        for j in media_id_list:

            group = df_[(df_.participant_id == i) & (df_.media_id == j)]
            if len(group) > 1:
                seq.append(pd.Series(range(len(group)), index=group.arousal.index))

    seq = pd.concat(seq)
    df_['seq'] = seq

    return df_
