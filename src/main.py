from amaai.classify import logistic_regression, getLabelsDict, makeLabelsIntegersBasedOnMap, svm, \
    gridSearchParameterEstimation
from amaai.openSmileFeatures import getOpenSmileFeatures
from amaai.visualise import visualise
import pandas as pd


# Demo/testing file, uncomment what you want to do.

# New (untested) features to test from essentia: getAllMusicFeatures('../testdata/')  will give time between beats + aggregates of it (mean, av., etc.)


extract_features = True

if extract_features == True:
    # loading features from test (we'd need to run this command twice, one for training set, one for test set.
    featurestraining = getOpenSmileFeatures('emobase', '../testdata/')
    featurestest = getOpenSmileFeatures('emobase', '../trainingdata/')


    # change labels to integer and create a label map
    labels_dict = getLabelsDict(featurestraining)
    featurestraining = makeLabelsIntegersBasedOnMap(featurestraining, labels_dict)
    featurestest = makeLabelsIntegersBasedOnMap(featurestest, labels_dict)

    # saving/loading features
    featurestraining.to_pickle('featurestraining')
    featurestest.to_pickle('featurestest')

else :
    featurestraining = pd.read_pickle('featurestraining')
    featurestest = pd.read_pickle('featurestest')




visualise(featurestraining, 'tsne', True)
visualise(featurestraining, 'explore', True)


y_train = featurestraining['class']
x_train = featurestraining.iloc[:,:len(featurestraining.columns)-2] # just removing the last two columns: filename and class

y_test = featurestest['class']
x_test = featurestest.iloc[:,:len(featurestest.columns)-2] # just removing the last two columns: filename and class


logistic_regression(x_train, y_train, x_test, y_test)

svm(x_train, y_train, x_test, y_test, 'rbf', 10, .00001)



# gridSearchParameterEstimation(x_train, y_train, x_test, y_test)

