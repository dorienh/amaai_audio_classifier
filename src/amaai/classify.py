from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC








def getLabelsDict(features):
    labels = features['class'].unique()
    labels_dict = dict(zip(labels, range(len(labels))))
    print('label dictionary: ')
    print(labels_dict)
    return labels_dict


def makeLabelsIntegersBasedOnMap(features, labels_dict):
    features = features.replace({"class": labels_dict})
    return features





def logistic_regression(x_train, y_train, x_test, y_test):

    logreg = LogisticRegression()
    logreg.fit(x_train, y_train)

    y_pred = logreg.predict(x_test)

    print('Accuracy of logistic regression classifier on test set: {:.2f}'.format(logreg.score(x_test, y_test)))

    print(classification_report(y_test, y_pred))

    logit_roc_auc = roc_auc_score(y_test, logreg.predict(x_test))
    fpr, tpr, thresholds = roc_curve(y_test, logreg.predict_proba(x_test)[:, 1])
    plt.figure()
    plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig('Log_ROC')
    plt.show()




def svm(x_train, y_train, x_test, y_test, thekernel, cost, thegamma):
    """
    :param x_train:
    :param y_train:
    :param x_test:
    :param y_test:
    :param thekernel: rbf, linear, ...
    :param cost: C
    :param thegamma:
    """

    print("SVM results: \n confusion matriX: \n")
    x_train_list = x_train.values
    y_train_list = y_train.values
    x_test_list = x_test.values
    y_test_list = y_test.values

    clf = SVC(kernel=thekernel, C=cost, gamma = thegamma)
    clf.fit(x_train, y_train_list)

    y_pred = clf.predict(x_test)

    print(confusion_matrix(y_test,y_pred))
    print(classification_report(y_test,y_pred))






def gridSearchParameterEstimation(x_train, y_train, x_test, y_test):

    # Grid Search
    # Parameter Grid
    param_grid = {'C': [0.1, 1, 10, 100], 'gamma': [1, 0.1, 0.01, 0.001, 0.00001, 10]}

    print("Finding best parameters for svm with grid search. ")

    x_train_list = x_train.values
    y_train_list = y_train.values

    # Make grid search classifier
    clf_grid = GridSearchCV(SVC(), param_grid, verbose=1)

    # Train the classifier
    clf_grid.fit(x_train, y_train)

    # clf = grid.best_estimator_()
    print("Best Parameters:\n", clf_grid.best_params_)
    print("Best Estimators:\n", clf_grid.best_estimator_)



