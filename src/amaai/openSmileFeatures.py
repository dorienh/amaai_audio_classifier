# import extractOpenSmile
import os
import shutil

import pandas as pd
from arff2pandas import a2p
import subprocess

from amaai.extractOpenSmile import extractFeatures





def getDataFrameFromCSV(filepath, sep, fileheader):
    # for each config:
    files = os.listdir(filepath)
    # print(files)
    for thisfile in files:
        contents = pd.read_csv(filepath + thisfile, sep=sep, header=None, names=fileheader)
    return contents


def getDataFrameFromCSVwithHeaderAndAggregate(filepath, sep):
    # TODO fix aggregation
    # for each config:
    files = os.listdir(filepath)
    allFeatures =[]
    # print(files)
    for thisfile in files:
        contents = pd.read_csv(filepath + thisfile, sep=sep)
        contents['filename'] = thisfile
        # todo for each line in contents aggregate it using aggregate(list) and add it to a new dataframe called aggregate_contents
        aggregate_contents = pd.DataFrame()
        allFeatures.append(aggregate_contents)
    return allFeatures



def fixEmoArff(fname):
    subprocess.call(["sed -i '' '993d' "+fname], shell=True)
    subprocess.call(["sed -i '' 's/,unassigned//g' "+fname], shell=True)



def getDataFrameFromARFF(filepath):
    # for each config:
    files = os.listdir(filepath)
    allFeatures = []
    # print(files)
    for thisfile in files:
        print(thisfile)
        fixEmoArff(filepath+thisfile)
        with open(filepath + thisfile) as f:
            contents = a2p.load(f)
            # add a column for filename
            contents['filename'] = thisfile
            allFeatures.append(contents)
        # print(contents)

    return allFeatures


def consolidate(featuresEmobaseDraft):
    if (len(featuresEmobaseDraft) < 1):
        print ("Error, feature files empty, most likely your chosen features require longer audio files.")
    else:
        jointdf = pd.concat(featuresEmobaseDraft)
    return jointdf




def checkPathExists(filepath):
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    else:
        for file in os.listdir(filepath):
            shutil.rmtree(filepath+file)
        # os.rmdir(filepath)
        print('Careful, '+ filepath + ' directory wasn\'t empty!')
        # os.makedirs(filepath)




def getChromaFeatures(datadir, workingdir, label):
    """
    Get chroma features, note, these are not yet consolidated per audio file, and stored in an csv file. TODO consolidation (average max, min etc. needs to be implemented)
    :param datadir: folder with audio files
    :param workingdir: where can we put the opensmile extracted feature files (should ideally be empty)
    :param label: foldername will stored as the class label
    :return:
    """
    checkPathExists(workingdir)
    checkPathExists(workingdir + 'chroma_fft')
    extractFeatures(datadir, workingdir + "chroma_fft/", "chroma_fft.conf")
    # load Chroma features
    header = ["chroma1", "chroma2", "chroma3", "chroma4", "chroma5", "chroma6", "chroma7", "chroma8", "chroma9", "chroma10", "chroma11", "chroma12"]
    featuresChroma = getDataFrameFromCSV(workingdir + 'chroma_fft', ";",header)
    # todo needs consolidating, max, min, avg, skewness, kurtosis, etc.
    featuresChroma['class'] = label
    return featuresChroma




def getEmobaseFeatures(datadir, workingdir, label):
    """
    Get emobase features, note, these are already consolidated per audio file, and stored in an arff file.
    :param datadir: folder with audio files
    :param workingdir: where can we put the opensmile extracted feature files (should ideally be empty)
    :param label: foldername will stored as the class label
    :return:
    """
    checkPathExists(workingdir)
    checkPathExists(workingdir + 'emobase')
    extractFeatures(datadir, workingdir + "emobase/", "emobase.conf")
    # load emobase features
    featuresEmobaseDraft = getDataFrameFromARFF('../OpenSmileFeatures/emobase/')
    featuresEmobase = consolidate(featuresEmobaseDraft)
    featuresEmobase['class'] = label

    # fix issue of not having index. (uncomment next line once featuresTest is ready)
    # featuresTest = featuresTest.reset_index(drop=True)

    return featuresEmobase




def getIS13Features(datadir, workingdir, label):
    """
    WARNING: audio files need to be long enough for this one to work.
    Get IS13 features, note, these are already consolidated per audio file, and stored in an arff file.
    :param datadir: folder with audio files
    :param workingdir: where can we put the opensmile extracted feature files (should ideally be empty)
    :param label: foldername will stored as the class label
    :return:
    """
    checkPathExists(workingdir)
    checkPathExists(workingdir + 'is13')
    extractFeatures(datadir, workingdir + "is13/", "IS13_ComParE_lld-func.conf")
    # load emobase features
    featuresIS13Draft = getDataFrameFromCSVwithHeader('../OpenSmileFeatures/is13/', ';')
    featuresIS13 = consolidate(featuresIS13Draft)
    featuresIS13['class'] = label

    # fix issue of not having index. (uncomment next line once featuresTest is ready)
    # featuresTest = featuresTest.reset_index(drop=True)

    return featuresIS13




def getOpenSmileFeatures(type, dir):
    """
    :param type: emobase, chroma, or others to indicate which opensmile config file to us
    :param dir: dir where the input data is located
    :return: pandas dataframe with features and label (directory name)
    """

    bufferdir = "../OpenSmileFeatures/";

    if (type == 'emobase'):


        allFeaturesList = []
        for dirs in os.walk(dir):
            for thisdir in dirs[1]:
                features  = getEmobaseFeatures(dirs[0] + '' + thisdir +'/', bufferdir, thisdir)
                allFeaturesList.append(features)

        featuresTest = consolidate(allFeaturesList)

        # remove columns with string values:
        featuresTest = featuresTest.iloc[:,2:]

        # fix issue of not having index.
        featuresTest = featuresTest.reset_index(drop=True)

    elif(type == 'IS13_ComParE_lld-func'):

        allFeaturesList = []
        for dirs in os.walk(dir):
            for thisdir in dirs[1]:
                features = getIS13Features(dirs[0] + '' + thisdir + '/', bufferdir, thisdir)
                allFeaturesList.append(features)

        featuresTest = consolidate(allFeaturesList)

        # remove columns with string values:
        featuresTest = featuresTest.iloc[:, 2:]

        # fix issue of not having index.
        featuresTest = featuresTest.reset_index(drop=True)



    print(featuresTest)


    # todo add other types of config files, e.g. getChromaFeatures is almost ready except for aggregation function


    return featuresTest







# functionals of all the features per file

# print(contents.Dataframe(1))
# mean1 = contents[1].mean()

