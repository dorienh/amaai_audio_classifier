import os
import numpy as np
import essentia as ess
import pandas as pd
import IPython
from scipy.stats import skew
from scipy.stats import kurtosis

from essentia.standard import *
# import libavresample



# time between detected beats
from amaai.openSmileFeatures import consolidate


def getBeatIntervals(file):
    # estimate beats with essentia
    audio = MonoLoader(filename=file)()
    rhythm_extractor = RhythmExtractor2013(method="multifeature")
    bpm, beats, beats_confidence, _, beats_intervals = rhythm_extractor(audio)
    return beats_intervals


def aggregate(list):
    nplist = np.asarray(list)
    # todo add some more aggregation:
    df = pd.DataFrame()
    df['average'] = np.average(nplist)
    df['var'] = np.var(nplist)
    df['skew'] = skew(nplist)
    df['kurtosis'] = kurtosis(nplist)
    df['max'] = np.max(nplist)
    df['min'] = np.min(nplist)
    df['range'] = np.range(nplist)
    return df



def getAllMusicFeatures(filepath):

    data = pd.Dataframe()

    files = os.listdir(filepath)
    allFeaturesList = []

    for thisfile in files:

        # first get time between beats
        beatFeatures = aggregate(getBeatIntervals(thisfile))
        beatFeatures['filename'] = thisfile
        beatFeatures['class'] = filepath

        # todo find more features, e.g.pitch class distribution, key, etc. add them here
        Features = beatFeatures

        allFeaturesList.append(Features)

    Features = consolidate(Features)

    # fix issue of not having index.
    Features = Features.reset_index(drop=True)

    return Features